@extends('layouts.app')
@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Create Idea</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(array('url' => url('/idea/store'), 'method' => 'post', 'class' => 'form-horizontal')) !!}

            {!! Form::bsText('name', 'Name', true, null, array('placeholder' => 'Idea Name')) !!}
            {!! Form::bsText('description', 'Description', true, null, array('placeholder' => 'Idea Description')) !!}
            {!! Form::bsRadio('Visibility', 1, array(
                array('name' => 'public', 'value' => 1, 'buttonLabel' => 'Public'),
                array('name' => 'public', 'value' => 0, 'buttonLabel' => 'Private')
                ), true) !!}
            {!! Form::bsSaveCancel('Create', 'Cancel') !!} {{-- Need to fix the cancel button to redirect back to index --}}

            {!! Form::close() !!}
        </div>
    </div>
@endsection