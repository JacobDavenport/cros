@extends('layouts.app')
@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title clearfix">
                <div class="text-center" style="font-size: 2em">
                    {{$idea->name}}
                    <a href="{{url('/idea/edit/' . $idea->id)}}" style="color: #2b542c; float: center; margin-left: 10px">
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </a>
                </div>
                <hr />
                <div class="text-center">
                    <p>{{ $idea->description }}</p>
                </div>
                <div class="btn-group pull-right" style="margin-top: -24px">
                    <a class="btn btn-primary btn-sm" data-target="#modal" data-toggle="modal"
                        data-title="New Suggestion" data-action="{{url('suggestion/create/' . $idea->id)}}">
                        New Suggestion</a>
                </div>
            </h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-6 text-center">
                    <h4>Pro's</h4>
                </div>
                <div class="col-md-6 text-center">
                    <h4>Con's</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 text-center">
                    <ul class="list-group" id="prosList">
                        @if(sizeof($pros) > 0)
                            @foreach($pros as $pro)
                                @if($pro->user->id == \Illuminate\Support\Facades\Auth::user()->id)
                                    <li class="list-group-item" id="suggestion{{$pro->id}}">
                                        {{$pro->description}}
                                        <a href="{{url('/suggestion/destroy/' . $pro->id)}}" style="color: #FF0000; float: left; margin-left: 10px">
                                            <i class="fa fa-times fa-3" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" style="color: #2b542c; float: left; margin-left: 10px"
                                           data-target="#modal" data-toggle="modal" data-title="Edit Suggestion" data-action="{{url('/suggestion/edit/' . $pro->id)}}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        <span class="badge">{{$pro->getImportance()}}</span>
                                    </li>
                                    @else
                                    <li class="list-group-item">
                                        {{$pro->description}}
                                        <span class="badge">{{$pro->getImportance()}}</span>
                                    </li>
                                @endif
                            @endforeach
                            @else
                            <h4 id="emptyProMessage">No pro's? Wanna be a nice person?</h4>
                        @endif
                    </ul>
                </div>
                <div class="col-md-6 text-center">
                    <ul class="list-group" id="consList">
                    @if(sizeof($cons) > 0)
                        @foreach($cons as $con)
                                @if($con->user->id == \Illuminate\Support\Facades\Auth::user()->id)
                                    <li class="list-group-item" id="suggestion{{$con->id}}">
                                        {{$con->description}}
                                        <a href="{{url('/suggestion/destroy/' . $con->id)}}" style="color: #FF0000; float: left; margin-left: 10px">
                                            <i class="fa fa-times" aria-hidden="true"></i>
                                        </a>
                                        <a href="#" style="color: #2b542c; float: left; margin-left: 10px"
                                            data-target="#modal" data-toggle="modal" data-title="Edit Suggestion" data-action="{{url('/suggestion/edit/' . $con->id)}}">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        <span class="badge">{{$con->getImportance()}}</span>
                                    </li>
                                @else
                                    <li class="list-group-item">
                                        {{$con->description}}
                                        <span class="badge">{{$con->getImportance()}}</span>
                                    </li>
                                @endif
                        @endforeach
                        @else
                        <h4 id="emptyConMessage">No con's? Wanna be a troll?</h4>
                    @endif
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">New Suggestion</h4>
                </div>
                <div class="modal-body">
                    <p>One fine body…</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary modal-submit">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/idea/view.js') }}"></script>
    <script type="text/javascript">
        var ideaID = '{{$idea->id}}';
    </script>
@endsection