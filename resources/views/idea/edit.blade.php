@extends('layouts.app')
@section('content')

    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Edit {!! $idea->name !!}</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(array('url' => url('/idea/update/' . $idea->id), 'method' => 'post', 'class' => 'form-horizontal')) !!}

            {!! Form::bsText('name', 'Name', true, $idea->name, array('placeholder' => 'Idea Name')) !!}
            {!! Form::bsText('description', 'Description', true, $idea->description, array('placeholder' => 'Idea Description')) !!}
            {!! Form::bsRadio('Visibility', $idea->public, array(
                array('name' => 'public', 'value' => 1, 'buttonLabel' => 'Public'),
                array('name' => 'public', 'value' => 0, 'buttonLabel' => 'Private')
                ), true) !!}
            {!! Form::bsSaveCancel('Save', 'Cancel') !!}

            {!! Form::close() !!}
        </div>
    </div>

@endsection