@extends('layouts.app')
@section('content')
    <h1 style="text-align: center;">Ideas</h1>
    <div>
        <ul class="nav nav-tabs">
            <li class="active"><a aria-expanded="true" href="#private" data-toggle="tab">Private</a></li>
            <li class=""><a aria-expanded="false" href="#public" data-toggle="tab">Public</a></li>
        </ul>
        <div id="myTabContent" class="tab-content">
            <div class="tab-pane fade active in" id="private">
                <div class="list-group">
                    <?php
                        $today = false;
                        $lastWeek = false;
                        $lastMonth = false;
                        $earlier = false;
                    ?>
                    @foreach($privateIdeas as $idea)
                        @if(!$today)
                            @if($idea->updated_at->gte($currentDay))
                                <h3 class="list-group-item"><strong>Today</strong></h3>
                                <?php
                                    $today = true;
                                    $iteration = $loop->iteration;
                                ?>
                            @endif
                        @endif
                        @if(!$lastWeek && $iteration != $loop->iteration)
                            @if($idea->updated_at->gte($sevenDays) && !$idea->updated_at->gte($currentDay))
                                <h3 class="list-group-item"><strong>Last Seven Days</strong></h3>
                                <?php
                                    $lastWeek = true;
                                    $iteration = $loop->iteration;
                                ?>
                            @endif
                        @endif
                        @if(!$lastMonth && $iteration != $loop->iteration)
                            @if($idea->updated_at->gte($thirtyDays) && !$idea->updated_at->gte($sevenDays))
                                <h3 class="list-group-item"><strong>Last Thirty Days</strong></h3>
                                <?php
                                $lastMonth = true;
                                $iteration = $loop->iteration;
                                ?>
                            @endif
                        @endif
                        @if(!$earlier && $iteration != $loop->iteration)
                            @if(!$idea->updated_at->gte($thirtyDays))
                                <h3 class="list-group-item"><strong>Earlier</strong></h3>
                                <?php
                                $earlier = true;
                                ?>
                            @endif
                        @endif
                        <a href="{{ URL::to('idea/view/' . $idea->id) }}" class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $idea->name }}</h4>
                            <p class="list-group-item-text">{{ $idea->description }}</p>
                        </a>
                    @endforeach
                </div>
            </div>
            <div class="tab-pane fade" id="public">
                <div class="list-group">
                    <?php
                    $today = false;
                    $lastWeek = false;
                    $lastMonth = false;
                    $earlier = false;
                    ?>
                    @foreach($publicIdeas as $idea)
                        @if(!$today)
                            @if($idea->updated_at->gte($currentDay))
                                <h3 class="list-group-item"><strong>Today</strong></h3>
                                <?php
                                $today = true;
                                $iteration = $loop->iteration;
                                ?>
                            @endif
                        @endif
                        @if(!$lastWeek && $iteration != $loop->iteration)
                            @if($idea->updated_at->gte($sevenDays) && !$idea->updated_at->gte($currentDay))
                                <h3 class="list-group-item"><strong>Last Seven Days</strong></h3>
                                <?php
                                $lastWeek = true;
                                $iteration = $loop->iteration;
                                ?>
                            @endif
                        @endif
                        @if(!$lastMonth && $iteration != $loop->iteration)
                            @if($idea->updated_at->gte($thirtyDays) && !$idea->updated_at->gte($sevenDays))
                                <h3 class="list-group-item"><strong>Last Thirty Days</strong></h3>
                                <?php
                                $lastMonth = true;
                                $iteration = $loop->iteration;
                                ?>
                            @endif
                        @endif
                        @if(!$earlier && $iteration != $loop->iteration)
                            @if(!$idea->updated_at->gte($thirtyDays))
                                <h3 class="list-group-item"><strong>Earlier</strong></h3>
                                <?php
                                $earlier = true;
                                ?>
                            @endif
                        @endif
                        <a href="{{ URL::to('idea/view/' . $idea->id) }}" class="list-group-item">
                            <h4 class="list-group-item-heading">{{ $idea->name }}</h4>
                            <p class="list-group-item-text">{{ $idea->description }}</p>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection