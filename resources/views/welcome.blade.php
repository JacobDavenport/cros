@extends('layouts.app')

@section('content')
    <style type="text/css">
        body{
            background: url({{asset('images/homepage-background.jpg')}});
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: 50% 25%;
            background-size: 100%;
        }

    </style>
    <div class="text-center" style="color:white; margin-top: -55px">
        <h1 style="color:white;">Welcome to Pro Idea!</h1>
        <p>Create, connect, and share ideas.</p>
    </div>
@endsection