<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <label class="control-label col-sm-2 {{($required) ? "required":""}}">{{$label}}</label>
    <div class="col-sm-6">
        <input type="password" class="form-control" name="{{$name}}" value="{{$value}}"
        @foreach($attributes as $attributeTag => $attributeValue)
            {{$attributeTag}}="{{$attributeValue}}"
        @endforeach/>

        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>