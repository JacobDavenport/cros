<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <div class="col-sm-6">
        <input type="hidden" class="form-control {{$classes}}" name="{{$name}}" value="{{(old($name)) ? old($name):$value}}"
        @foreach($attributes as $attributeTag => $attributeValue)
            {{$attributeTag}}="{{$attributeValue}}"
        @endforeach/>

        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>