<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <label class="control-label col-sm-2 {{($required) ? "required":""}}">{{$label}}</label>
    <div class="col-sm-6">
        <input type="text" class="form-control {{$classes}}" name="{{$name}}" value="{{(old($name)) ? old($name):$value}}"
        @foreach($attributes as $attributeTag => $attributeValue)
            {{$attributeTag}}="{{$attributeValue}}"
        @endforeach/>

        @if ($errors->has($name))
            <span class="help-block">
                <strong>{{ $errors->first($name) }}</strong>
            </span>
        @endif
    </div>
</div>
@if($classes == 'datepicker-input')
    <script type="text/javascript">
        $('.datepicker-input').datepicker(
                {
                    autoclose: true,
                    todayHighlight: true,
                    startDate: new Date('1900'),
                    format: 'yyyy-mm-dd'
                }
        );
    </script>
@endif