<div class="form-group {{ $errors->has($name) ? ' has-error' : '' }}">
    <label for="{{$name}}" class="control-label col-sm-2 {{($required) ? "required":""}} {{str_replace('[]', '', $name)}}">{{$label}}</label>
    <div class="col-sm-6">
        <select name="{{$name}}" id="{{str_replace('[]', '', $name)}}" class="form-control select2" value="{{(old($name)) ? old($name):$value}}"
        @foreach($attributes as $attributeTag => $attributeValue)
            {{$attributeTag}}="{{$attributeValue}}"
        @endforeach>
        <option></option>
            @foreach($options as $optionID => $option)
                <option value="{{$optionID}}">{{$option}}</option>
            @endforeach
        </select><?php echo ($icon) ? $icon:'' ?>

        @if ($errors->has($name))
            <span class="help-block">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
        @endif
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var element = $('#{{str_replace('[]', '', $name)}}');
        var jsonData = '<?php echo ($value) ? htmlspecialchars_decode($value, ENT_QUOTES):'' ?>';

        if(jsonData != '') {
            $('#{{str_replace('[]', '', $name)}}' + '> option').each(function () {
                var option = $(this);
                $.each($.parseJSON(jsonData), function (key, value) {
                    if(option.val() == value){
                        option.attr('selected', true);
                    }
                });
            });
        } else {
            $('#{{str_replace('[]', '', $name)}}' + '> option').each(function () {
                var option = $(this);
                if(option.val() != '') {
                    if (option.val() == '<?php echo old($name) ?>') {
                        option.attr('selected', true);
                    }
                }
            });
        }

        //init select2 element
        element.select2();
    });
</script>