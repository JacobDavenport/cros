<div class="form-group ">
    <label class="col-sm-2 control-label {{($required) ? "required":""}}">{{$label}}</label>
    <div class=" col-sm-6">
        <input name="{{$name}}" type="color" class="form-control" value="{{$value}}"
        @foreach($attributes as $attributeTag => $attributeValue)
            {{$attributeTag}}="{{$attributeValue}}"
        @endforeach>
    </div>
</div>