<div class="form-group">
    <label class="col-sm-2 control-label {{($required) ? "required":""}}">{{$label}}</label>
    <div class=" col-sm-8">
        @foreach($choices as $choice)
            <div class="radio">
                <label class="col-sm-6">
                    <input type="radio" name="{{$choice['name']}}" value="{{$choice['value']}}">
                    {{$choice['buttonLabel']}}
                </label>
            </div>
        @endforeach
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.col-sm-8 .radio input').each(function () {
            var input = $(this);
            if(input.val() == '<?php echo $value ?>') {
                input.attr('checked', true);
            }
        });
    });
</script>