<fieldset class="form-button-set col-sm-offset-2 hr-above form-horizontal">
    <button type="submit" name="button_group[submit]" class="btn-primary btn" value="save"><span class="pull-right button-right fa fa-floppy-o"></span> {{$save}}</button>
    <button type="button" name="button_group[cancel]" class="btn-link btn" value="Go Back"><span class="pull-right button-right fa fa-times"></span> {{$cancel}}</button>
</fieldset>