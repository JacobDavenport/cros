{!! Form::open(array('url' => url('/suggestion/update-suggestion/' . $suggestion->id), 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'editSuggestionForm')) !!}

{!! Form::bsRadio('Type', $suggestion->pro, array(
                array('name' => 'pro', 'value' => 1, 'buttonLabel' => 'Pro'),
                array('name' => 'pro', 'value' => 0, 'buttonLabel' => 'Con')
                ), true) !!}
{!! Form::bsTextArea('description', 'Description', true, $suggestion->description, array('placeholder' => 'Describe your suggestion')) !!}
{!! Form::bsRadio('Importance', $suggestion->importance, array(
                array('name' => 'importance', 'value' => 1, 'buttonLabel' => 'Very Unimportant'),
                array('name' => 'importance', 'value' => 2, 'buttonLabel' => 'Unimportant'),
                array('name' => 'importance', 'value' => 3, 'buttonLabel' => 'Neutral'),
                array('name' => 'importance', 'value' => 4, 'buttonLabel' => 'Important'),
                array('name' => 'importance', 'value' => 5, 'buttonLabel' => 'Very Important'),
                ), true) !!}

{!! Form::close() !!}