$(document).ready(function () {

    //edit the suggestion when the modal is triggered
    $("#modal").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var title = button.data('title'); // Extract info from data-* attributes
        var action = button.data('action'); // the location of the action we want to get our form from

        $.get(action, function (data) {
            $('.modal-body').html(data);
        });

        var modal = $(this); //variable containing the modal window
        modal.find('.modal-title').text(title); //set the modal window's title
    });

    //when the user submits the modal
    $('.modal-submit').click(function(e){
        var modal = $("#modal"); //get the modal
        var action = $('.form-horizontal').attr('action'); //get the action we are calling
        var form = $('.form-horizontal').serialize(); //get the data from the form

        if(typeof(action) == 'undefined'){
            modal.modal('toggle');
            enableSubmitButton();
        } else {
            //at this point we are ready to run a post call to the action specified by each element.
            $.post(action, form, function (data) {
                modal.find('.modal-body').html("<div class='loading'></div>");
                modal.find('.page-header').remove();
                modal.find('.form-button-set').remove();
                modal.find('.col-sm-2').addClass('col-sm-3').removeClass('col-sm-2');
                modal.find('.col-sm-6').addClass('col-sm-7').removeClass('col-sm-6');
                var callbackData = JSON.parse(data);

                if(callbackData.pro == '1')
                {
                    if(typeof (callbackData.selector) == 'undefined')
                    {
                        $('#emptyProMessage').hide();
                        $('#prosList').append(callbackData.html);
                        console.log(callbackData.html);
                    }
                    else
                    {
                        $(callbackData.selector).html(callbackData.html);
                    }
                }
                else if(callbackData.pro == '0')
                {
                    if(typeof (callbackData.selector) == 'undefined')
                    {
                        $('#emptyConMessage').hide();
                        $('#consList').append(callbackData.html);
                        console.log(callbackData.html);
                    }
                    else
                    {
                        console.log(callbackData.selector);
                        console.log($(callbackData.selector));
                        $(callbackData.selector).html(callbackData.html);
                    }

                }

                modal.modal('toggle');

            }).always(function(){
                //always renable the submit button, and change the text back
                enableSubmitButton();
            });
        }
    });

    var enableSubmitButton = function(){
        var submit = $('.modal-submit');
        submit.text(submit.data('text'));
        submit.prop("disabled", false);
    };
});