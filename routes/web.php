<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group(array('Suggestions' => 'SuggestionManagement'), function()
{
    Route::get('/suggestion', 'SuggestionController@index');
    Route::get('/suggestion/create/{idea}', 'SuggestionController@create');
    Route::get('/suggestion/edit/{suggestion}', 'SuggestionController@edit');
    Route::post('/suggestion/update-suggestion/{suggestion}', 'SuggestionController@updateSuggestion');
    Route::post('/suggestion/store/{idea}', 'SuggestionController@store');
    Route::get('/suggestion/destroy/{suggestion}', 'SuggestionController@destroy');
});

Route::group(array('Ideas' => 'IdeaManagement'), function()
{
    Route::get('/idea', 'IdeaController@index');
    Route::get('/idea/view/{idea}', 'IdeaController@view');
    Route::get('/idea/create', 'IdeaController@create');
    Route::post('/idea/store', 'IdeaController@store');
    Route::get('/idea/edit/{idea}', 'IdeaController@edit');
    Route::post('/idea/update/{idea}', 'IdeaController@update');
});