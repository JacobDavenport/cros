<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    
    /*
     * Gets all ideas the current user is allowed to access
     */
    public function ideas()
    {
        return $this->belongsToMany('App\Idea')->withTimestamps();
    }

    /*
     * Gets all ideas "owned" by the current user
     */
    public function ownedIdeas()
    {
        return $this->belongsToMany('App\Idea')->wherePivot('owner', '1')->withTimestamps();
    }

    /*
     * Gets all suggestions associated with the current user
     */
    public function suggestions()
    {
        return $this->hasMany('App\Suggestion');
    }
}
