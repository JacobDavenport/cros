<?php

namespace App\Http\Controllers;

use App\Suggestion;
use App\Idea;
use Illuminate\Support\Facades\Auth;

class SuggestionController extends Controller
{
    public function index()
    {
        return view('suggestion.index');
    }

    public function create(Idea $idea)
    {
        return view('suggestion.create', array('idea' => $idea));
    }

    public function store(Idea $idea)
    {
        $suggestion = new Suggestion();
        $suggestion->fill(request()->all());
        $suggestion->idea()->associate($idea);
        $suggestion->user()->associate(Auth::user()->id);
        $suggestion->save();

        $html = '<li class="list-group-item" id="suggestion' . $suggestion->id .  '">' . '<a href="' . url('/suggestion/destroy/' . $suggestion->id) . '" style="color: #FF0000; float: left;">' .
            '<i class="fa fa-times fa-3" aria-hidden="true"></i>' .
            '</a>' .
            '<a href="#" data-target="#modal" data-toggle="modal" data-title="Edit Suggestion" style="color: #2b542c; float: left; margin-left: 10px" data-action="' . url('/suggestion/edit/' . $suggestion->id) . '">' .
            '<i class="fa fa-pencil" aria-hidden="true"></i></a>' .
            $suggestion->description .
            '<span class="badge">' . $suggestion->getImportance() . '</span></li>';

        return json_encode(array('html' => $html, 'pro' => $suggestion->pro));
    }

    public function edit(Suggestion $suggestion)
    {
        return view('suggestion.edit', array('suggestion' => $suggestion));
    }

    public function updateSuggestion(Suggestion $suggestion)
    {
        $suggestion->fill(request()->all());
        $suggestion->save();

        $html = '<a href="' . url('/suggestion/destroy/' . $suggestion->id) . '" style="color: #FF0000; float: left;">' .
            '<i class="fa fa-times fa-3" aria-hidden="true"></i>' .
            '</a>' .
            '<a href="#" data-target="#modal" data-toggle="modal" data-title="Edit Suggestion"
                style="color: #2b542c; float: left; margin-left: 10px"
                 data-action="' . url('/suggestion/edit/' . $suggestion->id) . '">' .
            '<i class="fa fa-pencil" aria-hidden="true"></i></a>' .
            $suggestion->description .
            '<span class="badge">' . $suggestion->getImportance() . '</span>';

        return json_encode(array('html' => $html, 'pro' => $suggestion->pro, 'selector' => '#suggestion' . $suggestion->id));
    }

    public function destroy(Suggestion $suggestion)
    {
        $suggestion->delete();
        return redirect(url('/idea/' . $suggestion->idea->id));
    }
}