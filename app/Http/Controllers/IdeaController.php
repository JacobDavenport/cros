<?php

namespace App\Http\Controllers;
use App\Idea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Suggestion;
use Carbon\Carbon;

class IdeaController extends Controller
{
    public function index()
    {
        $publicIdeas =  Idea::where('public', '=', '1')->orderBy('updated_at', 'desc')->get();
        $privateIdeas = Auth::user()->ideas()->orderBy('updated_at', 'desc')->get();
        $currentDay = Carbon::today();
        $sevenDays = Carbon::today()->subDays(7);
        $thirtyDays = Carbon::today()->subDays(30);
        return view('idea.index', compact('publicIdeas', 'privateIdeas', 'currentDay', 'sevenDays', 'thirtyDays'));
    }

    //return the form view to create a new idea
    //this may be added to a modal window in the future
    public function create()
    {
        return view('idea.create');
    }

    public function view(Idea $idea)
    {
        $idea->updated_at = Carbon::now()->format('Y-m-d H:i:s');
        $idea->save();
        $proList = Suggestion::where('pro', '=', true)->where('idea_id', '=', $idea->id)->get();
        $conList = Suggestion::where('pro', '=', false)->where('idea_id', '=', $idea->id)->get();

        return view('idea.view', array('idea' => $idea, 'pros' => $proList, 'cons' => $conList));
    }

    //store newly created idea in the db with the pivot table being populated
    //need to make sure that the 'owner' field in the pivot table is set to 1 on creation
    public function store()
    {
        $idea = new Idea(request()->all());
        $user = Auth::user();
        $idea->save();
        $idea->users()->attach($user);
        //set this user to owner in pivot table here
        return redirect(url('/idea'));
    }

    public function destroy(Idea $idea)
    {
        $idea->delete();
        return redirect('/idea');
    }

    public function edit(Idea $idea)
    {
        return view('idea.edit', compact('idea'));
    }

    public function update(Idea $idea)
    {
        $idea->update(request()->all());
        return redirect(url('/idea'));
    }
}