<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idea extends Model
{
    protected $fillable = [
        'name', 'description', 'public'
    ];

    /*
     * Gets all users associated with the current idea
     */
    public function users()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    /*
     * Gets all users who own the current idea
     */
    public function owners()
    {
        return $this->belongsToMany('App\User', 'idea_user', 'idea_id', 'owner_id')->withTimestamps();
    }

    public function suggestions()
    {
        return $this->hasMany('App\Suggestion');
    }
}
