<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $fillable = [
        'pro', 'importance', 'description'
    ];

    /*
     * Get the users associated with the current suggestion
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getImportance()
    {
        $importanceText = '';
        switch($this->importance)
        {
            case 1:
                $importanceText = 'Very Unimportant';
                break;
            case 2:
                $importanceText = 'Unimportant';
                break;
            case 3:
                $importanceText = 'Neutral';
                break;
            case 4:
                $importanceText = 'Important';
                break;
            case 5:
                $importanceText = 'Very Important';
                break;
        }
        return $importanceText;
    }
    
    public function idea()
    {
        return $this->belongsTo('App\Idea');
    }
}
