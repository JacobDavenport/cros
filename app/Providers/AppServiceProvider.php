<?php

namespace App\Providers;

use Collective\Html\FormFacade;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        FormFacade::component('bsSelect2', 'components.form.select2', array('name', 'label', 'required', 'value' => null, 'options' => array(), 'attributes' => array(), 'icon' => null));
        FormFacade::component('bsText', 'components.form.text', array('name', 'label', 'required', 'value' => null, 'attributes' => array(), 'classes' => null));
        FormFacade::component('bsNumber', 'components.form.number', array('name', 'label', 'required', 'value' => null, 'attributes' => array(), 'classes' => null));
        FormFacade::component('bsHidden', 'components.form.hidden', array('name', 'label', 'required', 'value' => null, 'attributes' => array(), 'classes' => null));
        FormFacade::component('bsPasswordText', 'components.form.password', array('name', 'label', 'required', 'value' => null, 'attributes' => array()));
        FormFacade::component('bsTextArea', 'components.form.textArea', array('name', 'label', 'required', 'value' => null, 'attributes' => array(), 'classes' => null));
        FormFacade::component('bsColorPicker', 'components.form.colorPicker', array('name', 'label', 'required', 'value' => null, 'attributes' => array()));
        FormFacade::component('bsSaveCancel', 'components.form.saveCancel', array('save', 'cancel'));
        FormFacade::component('bsRadio', 'components.form.radio', array('label', 'value' => null, 'choices' => array(), 'required'));
        FormFacade::component('bsFile', 'components.form.file', array('name', 'label', 'required', 'classes' => null, 'attributes' => array()));
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
